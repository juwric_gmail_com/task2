﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;

public class TransactionInfo
{
    public string VehicleId { get; set; }
    public DateTime Dt { get; set; }
    public decimal Sum { get; set; }

    public override string ToString()
    {
        return $"{Dt.ToLongTimeString()}: ${VehicleId} - ${Sum}";
    }
}